/****************************************************
  ESP8266 CSS811 compensated via BME280 with MQTT
  2018, Patrik Mayer - patrik.mayer@codm.de

  mqtt client from https://github.com/knolleary/pubsubclient/
  Arduino OTA from ESP8266 Arduino Core V2.4
  Adafruit CCS811 from https://github.com/adafruit/Adafruit_CCS811
  Sparkfun BME280 from https://github.com/sparkfun/SparkFun_CCS811_Arduino_Library
  Tasker from https://github.com/sticilface/Tasker

  Connect the sensors via I2C, (SDA -> GPIO4 (D2)/ SCL -> GPIO5(D1)). Don't forget
  the I2C Pull-Ups.
  The used Adafruit CCS811 library expects I2C address 0x5A, so configure your
  CCS811 accordingly.
  !WAKE is currently not used.
  Serial is left open for debugging (115200).

  The following topics will be dropped to mqtt, all retained:

   <mqttTopicPrefix>status          online/offline (last will)
   <mqttTopicPrefix>ip              system ip
   <mqttTopicPrefix>temperature     temperature in °C
   <mqttTopicPrefix>humidity        relative humidity in %
   <mqttTopicPrefix>pressure        pressure in hPa
   <mqttTopicPrefix>altitude        altitude in m
   <mqttTopicPrefix>co2             CO2 concentration in ppm
   <mqttTopicPrefix>tvoc            total volatile compound in ppb

  If you want to read Farenheit/Feet change readTempC() to readTempF() and
  readFloatAltitudeMeters() to readFloatAltitudeFeet().
  ccs811.setEnvironmentalData() expects the temperature as °C

  ArduinoOTA is enabled, the mqttClientName is used as OTA name, see config.

****************************************************/

#include <Tasker.h>
#include <Wire.h>

#include "Adafruit_CCS811.h"
#include <SparkFunBME280.h>

#include "SSD1306.h"



/*
 * Main Settings
 *
 * measureDelay: defines how often data will be pulled from the sensors (in ms, means 60000 = 60s)
 */
const unsigned long measureDelay = 60000;
/*
 * GPIO Settings
 *
 * LED_BUILTIN: Digial Pin Number of your boards built-in LED or
 *               the LED you wish to show status signals
 */
#define LED_BUILTIN 2 //ESP-12F has the builtin LED on GPIO2, comment for other boards

/*
 * CCS811
 *
 * WAKE_PIN: Digial Pin Number, where your BME280s wake signal is connected
 */
#define WAKE_PIN D0
#define RST_PIN D6

// internal vars
Adafruit_CCS811 ccs811;
BME280 myBME280;

                  // Initialize the OLED display using Wire library
SSD1306  display(0x3c, D2, D1); // D2 -> SDA D1 ->SCL

Tasker tasker;

float bme280TemperatureC;
float bme280Humidity;
float bme280Pressure;
float bme280Altitude;

uint16_t ccs811co2;
uint16_t ccs811tvoc;

bool _hb = true;

void setup() {
  Serial.begin(115200);
  Serial.println("\n\rStarting up...");

  pinMode(LED_BUILTIN, OUTPUT);

  // Initialising the display.
  display.init();

  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);
  display.clear();
  DisplayWelcomme();

  // reset sensor
  pinMode(RST_PIN, OUTPUT);             // set RST pin as OUTPUT
  digitalWrite(RST_PIN, LOW);
  delay(1);
  digitalWrite(RST_PIN, HIGH);
  // wake_up sensor
  pinMode(WAKE_PIN, OUTPUT);   // set WAKE pin as OUTPUT
  digitalWrite(WAKE_PIN, LOW);  // WAKE_PIN on the sensor is active low, must always be asserted before any communication and held low throughout

  if (!ccs811.begin()) {
    Serial.println("CCS811 Initialization failed.");
    while (1);
  }

  //wait for ccs811 to be available
  while(!ccs811.available());

  //Initialize BME280
  myBME280.settings.commInterface = I2C_MODE;
  myBME280.settings.I2CAddress = 0x76; // 0x77
  myBME280.settings.runMode = 3; //Normal mode
  myBME280.settings.tStandby = 0;
  myBME280.settings.filter = 4;
  myBME280.settings.tempOverSample = 5;
  myBME280.settings.pressOverSample = 5;
  myBME280.settings.humidOverSample = 5;

  //Calling .begin() causes the settings to be loaded
  delay(10);  //Make sure sensor had enough time to turn on. BME280 requires 2ms to start up.

  if (!myBME280.begin()) {
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    while (1);
  }

  Serial.println("ready...");
  digitalWrite(LED_BUILTIN, LOW);
  delay(500);
  //ready, blink the led twice
  for (uint8_t i = 0; i < 5; i++ ) {
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
    delay(200);
  }

  //measure every <measureDelay>ms - GO!
  tasker.setInterval(meassureEnvironment, measureDelay);
}

void loop() {
  tasker.loop();
}

void meassureEnvironment() {
  bme280TemperatureC = myBME280.readTempC();;
  bme280Humidity = myBME280.readFloatHumidity();
  bme280Pressure = myBME280.readFloatPressure();
  bme280Altitude = myBME280.readFloatAltitudeMeters();

  Serial.print("Temperature = ");
  Serial.print(bme280TemperatureC);
  Serial.println(" *C");

  Serial.print("Pressure = ");
  Serial.printf("%.0f", bme280Pressure/100);
  Serial.println(" hPa");

  Serial.print("Approx. Altitude = ");
  Serial.print(bme280Altitude);
  Serial.println(" m");

  Serial.print("Humidity = ");
  Serial.print(bme280Humidity);
  Serial.println(" %");


  ccs811.setEnvironmentalData(bme280Humidity, bme280TemperatureC);
  //wait for ccs811 reading
  while(ccs811.readData());

  ccs811co2 = ccs811.geteCO2();
  ccs811tvoc = ccs811.getTVOC();

  Serial.print("CO2 concentration : ");
  Serial.print(ccs811co2);
  Serial.println(" ppm");

  Serial.print("TVOC concentration : ");
  Serial.print(ccs811tvoc);
  Serial.println(" ppb");

  // Oled Display

  display.clear();
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_10);
  display.drawString(0, 10, "CO2 : " + String(ccs811co2) +" ppm" );
  display.drawString(0, 20, "COV : "+ String(ccs811tvoc)+  " ppb");
  display.drawString(0, 30, "Temp : "+ String(bme280TemperatureC)+  " °C");
  display.drawString(0, 40, "Humid : "+ String(bme280Humidity)+  " %");
  display.drawString(0, 50, "Press : "+ String(bme280Pressure/100)+  " hPa");
  display.display();

  Serial.println();
}

void DisplayWelcomme() {
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.setFont(ArialMT_Plain_24);
  display.drawString(64, 18, "CaptAir");
  display.display();
}
