/***************************************************
  This is an example for the CCS811 digital TVOC/eCO2 Sensor by CCMOSS/AMS
  http://www.ccmoss.com/gas-sensors#CCS811

  Updated: March 28, 2017

  The sensor uses I2C protocol to communicate, and requires 2 pins - SDA and SCL
  Another GPIO is also required to assert the WAKE pin for communication. this
  pin is passed by an argument in the begin function.

  A breakout board is available: https://github.com/AKstudios/CCS811-Breakout

  The ADDR pin on the sensor can be connected to VCC to set the address as 0x5A.
  The ADDR pin on the sensor can be connected to GND to set the address as 0x5B.

  Written by Akram Ali from AKstudios (www.akstudios.com)
  GitHub: https://github.com/AKstudios/
 ****************************************************/

#include "CCS811.h"
#include <BME280I2C.h>
#include "SSD1306.h"


#define ADDR      0x5A
//#define ADDR      0x5B
#define WAKE_PIN  D0
#define RST_PIN D6

CCS811 sensor;
BME280I2C bme;    // Default : forced mode, standby time = 1000 ms
                  // Oversampling = pressure ×1, temperature ×1, humidity ×1, filter off,

                  // Initialize the OLED display using Wire library
SSD1306  display(0x3c, D2, D1); // D2 -> SDA D3 ->SCL

bool _hb = false;

void setup()
{
  Serial.begin(115200);

  // Initialising the display.
  display.init();

  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);
  display.clear();
  DisplayWelcomme();
  display.display();

  // reset sensor
  pinMode(RST_PIN, OUTPUT);             // set RST pin as OUTPUT
  digitalWrite(RST_PIN, LOW);
  delay(50);
  digitalWrite(RST_PIN, HIGH);
  Wire.begin();

  while(!bme.begin())
  {
    Serial.println("Could not find BME280 sensor!");
    delay(1000);
  }

  switch(bme.chipModel())
  {
     case BME280::ChipModel_BME280:
       Serial.println("Found BME280 sensor! Success.");
       break;
     case BME280::ChipModel_BMP280:
       Serial.println("Found BMP280 sensor! No Humidity available.");
       break;
     default:
       Serial.println("Found UNKNOWN sensor! Error!");
  }


  Serial.println("CCS811 test");
  Wire.begin();
  if(!sensor.begin(uint8_t(ADDR), uint8_t(WAKE_PIN))) {
    Serial.println("Initialization failed.");
  }
  sensor.readCO2(); // init reading
  sensor.readTVOC(); //
}

void loop()
{
  float pres, temp, hum;
  int CO2, TVOC;

  BME280::TempUnit tempUnit(BME280::TempUnit_Celsius);
  BME280::PresUnit presUnit(BME280::PresUnit_hPa);

  bme.read(pres, temp, hum, tempUnit, presUnit);

  sensor.compensate(temp, hum);
  Serial.print(sensor.getData());Serial.print("\t");

  CO2 = sensor.readCO2();
  TVOC = sensor.readTVOC();

  Serial.print("temp: "); Serial.print(temp); Serial.print(" °C");
  Serial.print("\tPression: "); Serial.print(pres); Serial.print(" hPa");

  Serial.print("\tHumidité: "); Serial.print(hum); Serial.print(" %");

  //sensor.compensate(22.56, 30.73);  // replace with t and rh values from sensor
  Serial.print("\tCO2 : "); Serial.print(CO2); Serial.print(" ppm");
  Serial.print("\tTVOC : "); Serial.print(TVOC); Serial.println(" ppb");
  //Serial.println();

  Display_Data(CO2, TVOC, temp, hum, pres);

  delay(2000);
}

void DisplayWelcomme() {
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.setFont(ArialMT_Plain_24);
  display.drawString(64, 18, "CaptAir");
}

void Display_Data(int CO2, int TVOC, float temp, float hum, float pres) {

  String to_display;

  display.clear();
  display.setTextAlignment(TEXT_ALIGN_RIGHT);
  if (_hb) display.drawString(155, 10,"#");
  else display.drawString(155, 10," ");

  _hb =  !_hb;

  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_10);
  to_display = "CO2 : " + String(CO2) +" ppm";
  display.drawString(0, 10, to_display);
  to_display = "TVOC : "+ String(TVOC)+  " ppb";
  display.drawString(0, 20, to_display);
  to_display = "Temp : "+ String(temp)+  " °C";
  display.drawString(0, 30, to_display);
  to_display = "Humid : "+ String(hum)+  " %";
  display.drawString(0, 40, to_display);
  to_display = "Press : "+ String(pres)+  " hPa";
  display.drawString(0, 50, to_display);
  display.display();
}



